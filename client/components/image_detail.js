import React from 'react';
import ImageScore from './image_score';

const ImageDetail = (props) => {
    return (
        <li className="media list-group-item">
            {/*styles image and title + progress bar to look cool uses twitter bootstrap*/}
            <div className="media-left"><img src={props.image.link}/></div>
            <div className="media-body">
                <h4 className="media-heading">{props.image.title}</h4>
                <p>{props.image.description}</p>
                {/*sends up- and downvotes data to calculate the width of the progress bar */}
                <ImageScore ups={props.image.ups} downs={props.image.downs}/>
            </div>
        </li>
    );
};

export default ImageDetail;