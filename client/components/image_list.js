import React from 'react';
import ImageDetail from './image_detail';

const ImageList = (props) => {
    /**
     * @type {Array.<T>}
     * is_album is object in the array retrieved from imgur.
     * filters out the files which are albums
     */
    const validImages = props.images.filter(image => !image.is_album);

    const RenderedImages = validImages.map(image => {
        return <ImageDetail key={image.title} image={image}/>
    });

    return (
        <ul className="media-list list-group">
            {RenderedImages}
        </ul>
    );
};

export default ImageList;