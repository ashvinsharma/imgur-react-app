import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import ImageList from './components/image_list';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {images: []};
        // whenever the state changes, render() function runs again
    }

    componentWillMount() {
//      Loads before the render()...
        axios.get('https://api.imgur.com/3/gallery/hot/viral/0')
            .then(response => this.setState({images: response.data.data}));
    //        makes render reload again when the state is changed/ images array is filled
    }

    render() {
        return (
            <div>
                <ImageList images={this.state.images}/>
            </div>
        );
    }
}

Meteor.startup(() => {
    ReactDOM.render(<App/>, document.querySelector('.container'));
});
